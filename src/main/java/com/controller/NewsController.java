package com.controller;

import com.domain.News;
import com.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Алексей on 02.04.2016.
 */
@Controller
public class NewsController {
    @Autowired
    NewsRepository newsRepository;
    @RequestMapping("/string")
    public @ResponseBody String string(){return new String("223243532DAS");}
    @RequestMapping(value = "/news", method = RequestMethod.GET)
   public @ResponseBody
    Iterable<News> list() {
       List<News> news=newsRepository.findAll();
      return news;
    }
    @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
    public @ResponseBody News list(@PathVariable("id") Integer id) {
        return newsRepository.findOne(id);
    }
   // @RequestMapping(value = "/news1", method = RequestMethod.GET)
   // public Iterable<News> lost() {
   //     List<News> news=newsRepository.findById(1);
    //    return news;
    //}
}
