package com.domain;

import javax.persistence.*;

/**
 * Created by Алексей on 02.04.2016.
 */
@Entity
@Table(name = "news")
public class News {
    @Id
    private Integer id;
    private String title;
    private String content;
    private String create_at_date;

    public String getTitle() {
        return title;
    }

    public News setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public News setContent(String content) {
        this.content = content;
        return this;
    }

    public String getCreate_at_date() {
        return create_at_date;
    }

    public News setCreate_at_date(String create_at_date) {
        this.create_at_date = create_at_date;
        return this;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;

        News news = (News) o;

        if (!id.equals(news.id)) return false;
        if (!title.equals(news.title)) return false;
        if (!content.equals(news.content)) return false;
        return create_at_date.equals(news.create_at_date);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + create_at_date.hashCode();
        return result;
    }

    public News setId(Integer id) {
        this.id = id;
        return this;
    }

}
