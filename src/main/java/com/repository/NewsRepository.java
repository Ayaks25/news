package com.repository;

import com.domain.News;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Алексей on 02.04.2016.
 */
//@RepositoryRestResource(collectionResourceRel = "news", path = "news")
@Repository
public interface NewsRepository extends CrudRepository<News,Integer> {
    List<News> findAll();
    List<News> findById(Integer id);
}