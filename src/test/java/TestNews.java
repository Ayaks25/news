/**
 * Created by aleksey on 11.04.16.
 */
import com.Application;
import com.domain.News;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.repository.NewsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest(randomPort = true)
@SpringApplicationConfiguration(classes = Application.class)
public class TestNews {
    @Autowired
    NewsRepository newsRepository;

    @Value("${local.server.port}")
    int port;

    @Before
    public void init(){
        RestAssured.port = port;
        //RestAssured.basePath = "/news";
        //RestAssured.authentication = basic("username", "password");
        //RestAssured.rootPath = "x.y.z";
    }

    @Test
    public void testNews() throws Exception{
        News a=new News();
        a.setId(1)
         .setContent("1")
         .setCreate_at_date("1");
        a.setTitle("1");
        News one = newsRepository.findOne(1);
        assertEquals(a,one);
    }
    @Test
    public void testPort() throws Exception{
        given().
                log().all().
                contentType(ContentType.JSON).
        when().
                get("news/1").
        then().
                log().all().
                statusCode(200);
    }
    @Test
    public void testJson() throws Exception{
        given().
                log().all().
                contentType(ContentType.JSON).
                when().
                get("news/1").
                then().
                log().all().
                assertThat().body("id", equalTo(1));
    }
}